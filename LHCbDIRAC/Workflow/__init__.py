############################################################

"""
   LHCbDIRAC.Interfaces package

   This contains the LHCb specific API plugins for DIRAC.
"""

__RCSID__ = "$Id$"
