############################################################

"""
   LHCbSystem.Agent package

   LHCb specific  agents for any system (e.g. WMS, DMS ...) can be added here.
"""

__RCSID__ = "$Id$"
